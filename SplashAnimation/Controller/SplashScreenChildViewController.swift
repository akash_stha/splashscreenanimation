//
//  SplashScreenChildViewController.swift
//  SplashAnimation
//
//  Created by Newarpunk on 9/7/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

class SplashScreenChildViewController: UIViewController, UIViewControllerTransitioningDelegate {
    
    var circleAnimator: CircleAnimator?
            
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(dismissSplashScreen), userInfo: nil, repeats: false)
    }

    @objc func dismissSplashScreen() {
        self.dismiss(animated: true)
    }

}

