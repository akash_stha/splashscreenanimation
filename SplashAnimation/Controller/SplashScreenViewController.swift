//
//  SplashScreenViewController.swift
//  SplashAnimation
//
//  Created by Newarpunk on 9/7/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var button: UIButton!
    
    var circleAnimator: CircleAnimator?
        
    override func viewDidLoad() {
        super.viewDidLoad()
    }
        
    override func viewDidAppear(_ animated: Bool) {
        buttonTapped(nil)
        Timer.scheduledTimer(timeInterval: 3.9, target: self, selector: #selector(callMainMenu), userInfo: nil, repeats: false)
    }
    
    @objc func callMainMenu() {
        let mainVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController")
        self.view.window?.rootViewController = mainVC
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard let backgroundColor = button.backgroundColor else { return circleAnimator }
        circleAnimator = CircleAnimator(view: button, color: backgroundColor, duration: 0.4)
        circleAnimator?.mode = .dismiss
        return circleAnimator
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard let backgroundColor = button.backgroundColor else { return circleAnimator }
        circleAnimator = CircleAnimator(view: button, color: backgroundColor, duration: 0.5)
        circleAnimator?.mode = .present
        
        return circleAnimator
    }

    @IBAction func buttonTapped(_ sender: UIButton?) {
        let splashChildVC = UIStoryboard.init(name: "SplashScreenChild", bundle: nil).instantiateViewController(withIdentifier: "SplashScreenChildViewController") as! SplashScreenChildViewController
        splashChildVC.transitioningDelegate = self
        splashChildVC.modalPresentationStyle = .custom
        self.present(splashChildVC, animated: true, completion: nil)
    }
    
}
